export const blue = {

  '--primary' : '#68c6ff',
  '--primary-variant': '#1e1e1e',

  '--secondary': '#007bbf',
  '--secondary-variant': '#007bbf',

  '--background': '#141414',
  '--surface': '#1e1e1e',
  '--dialog': '#000000',
  '--alt-surface': '#323232',
  '--alt-dialog': '#455a64',
  '--cancel': '#9E9E9E',

  '--on-primary': '#000000',
  '--on-secondary': '#ffffff',
  '--on-background': '#FFFFFF',
  '--on-surface': '#FFFFFF',
  '--on-cancel': '#000000',

  '--green': 'lightgreen',
  '--red': 'red',
  '--yellow': 'yellow',
  '--blue': 'blue',
  '--purple': 'purple',
  '--light-green': 'lightgreen',
  '--grey': 'grey',
  '--grey-light': 'lightgrey',
  '--black': 'black',
  '--moderator': '#37474f'

};

export const blue_meta = {

  'translation': {
    'name': {
      'en': 'Harmonious Blue',
      'de': 'Harmonisches Blau'
    },
    'description': {
      'en': 'Atmospheric design according to WCAG 2.1 AA',
      'de': 'Atmosphärisches Design nach WCAG 2.1 AA'
    }
  },
  'isDark': true,
  'order': 4,
  'scale_desktop': 1,
  'scale_mobile': 1,
  'previewColor': 'secondary'

};
